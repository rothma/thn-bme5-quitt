package de.ohmhs.bme.quitt.controller;

import de.ohmhs.bme.quitt.dao.PaygroupRepository;
import de.ohmhs.bme.quitt.dao.QuserRepository;
import de.ohmhs.bme.quitt.model.Paygroup;
import de.ohmhs.bme.quitt.model.Payment;
import io.javalin.http.Context;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Controller implementation for paygroup related actions. All methods take a {@link Context} instance as a
 * parameter in order to consume request information and rendering an output.
 * <p>
 * In this case rendering dynamic HTML is performed with thymeleaf templates.
 *
 * @see <a href="https://www.thymeleaf.org">thymeleaf templating engine</a>
 */
public class PaygroupController extends AbstractController {

    private PaygroupRepository repo;
    private QuserRepository userRepos;

    /**
     * Constructor that takes repositories needed for database operations
     *
     * @param paygroupRepo an paygroupRepository instance
     * @param userRepo     an QuserRepository instance
     */
    public PaygroupController(PaygroupRepository paygroupRepo, QuserRepository userRepo) {
        this.repo = paygroupRepo;
        this.userRepos = userRepo;
    }

    /**
     * lookup of paygroups where currently logged in user is member of and rendering the list
     * in groups.html thymeleaf template.
     *
     * @param ctx the Javalin Context instance
     */
    public void handleGetGroups(Context ctx) {
        try {
            ctx.render(
                    "/groups.html",
                    Map.of(
                            "payGroups",
                            repo.getPaygroupsForUsername(ctx.sessionAttribute("currentuser"))));
        } catch (Throwable throwable) {
            handleError(ctx, throwable);
        }
    }

    /**
     * lookup of specific paygroup (dynamic path param), calculating the actual balance of the current user
     * and rendering the detail view in balance.html thymeleaf template
     *
     * @param ctx the Javalin Context instance
     */
    public void handleGetPaygroup(Context ctx) {
        try {
            Paygroup pg = repo.getPaygroupForTitle(ctx.pathParam("title"));
            if (!pg.getUsers().contains(ctx.sessionAttribute("currentuser"))) {
                ctx.status(401).result("not found");
            } else {
                Float balance = calculateBalanceForUser(pg, ctx.sessionAttribute("currentuser"));
                ctx.render(
                        "/balance.html",
                        Map.of(
                                "payments",
                                pg.getTransactions(),
                                "balance",
                                balance,
                                "groupName",
                                pg.getTitle()
                        )
                );
            }
        } catch (Throwable throwable) {
            handleError(ctx, throwable);
        }
    }

    /**
     * renders a form from thymeleaf template add_group.html
     * for creating a new paygroup with a populated list of all known users in the database to
     * add them as members.
     *
     * @param ctx the Javalin Context instance
     */
    public void handleShowAddPaygroup(Context ctx) {
        try {
            ctx.render("/add_group.html", Map.of("users", userRepos.findAll()));
        } catch (Throwable throwable) {
            handleError(ctx, throwable);
        }
    }

    /**
     * consumes a form post from the 'add paygroup' form rendered in handleAddPaygroup
     * adds the currently logged in user as a member and saves it via {@link PaygroupRepository}
     * to database. After that a redirect to the application root ('/') is returned where
     * the new paygroup is shown as a new list entry.
     *
     * @param ctx the Javalin Context instance
     */
    public void handleAddPaygroup(Context ctx) {
        if (ctx.formParam("title") == null ||
                ctx.formParam("title").isEmpty() ||
                ctx.formParams("members").isEmpty()) {
            ctx.render("/");
        } else {
            List<String> members = new ArrayList<>(ctx.formParams("members"));
            Paygroup pg = new Paygroup();
            pg.setTitle(ctx.formParam("title"));
            if (!members.contains(ctx.sessionAttribute("currentuser"))) {
                members.add(ctx.sessionAttribute("currentuser"));
            }
            pg.setUsers(members);
            try {
                repo.create(pg);
                ctx.redirect("/");
            } catch (Throwable throwable) {
                handleError(ctx, throwable);
            }
        }
    }

    /**
     * renders a form from thymeleaf template add_money.html
     * for adding a payment to a paygroup.
     *
     * @param ctx the Javalin Context instance
     */
    public void handleGetMoney(Context ctx) {
        ctx.render("/add_money.html", Map.of("groupname", ctx.pathParam("title")));
    }

    /**
     * consumes a form post from the 'add payment' form rendered in handleGetMoney.
     * It creates a new {@link Payment} instance, populates it with the appropriate information
     * looks up the paygroup in the database, adds the new payment to it and updates the paygroup document in database
     *
     * @param ctx the Javalin Context instance
     */
    public void handleAddMoney(Context ctx) {
        if (ctx.formParam("amount") == null
                || ctx.formParam("amount").isEmpty()) {
            handleGetMoney(ctx);
        } else {
            Payment p = new Payment();
            p.setUser(ctx.sessionAttribute("currentuser"));
            p.setDate(new Date());
            p.setAmount(BigDecimal.valueOf(Long.parseLong(ctx.formParam("amount"))));
            try {
                Paygroup pg = repo.getPaygroupForTitle(ctx.pathParam("title"));
                pg.getTransactions().add(p);
                repo.save("title", pg.getTitle(), pg);
                ctx.redirect("/balance/" + pg.getTitle());
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                handleError(ctx, throwable);
            }
        }
    }

    /**
     * helper function that calculates the current balance for a given user and paygroup
     *
     * @param pg       paygroup where the payments are saved
     * @param username the user whose balance should be calculated
     * @return the current balance as Float value
     */
    private Float calculateBalanceForUser(Paygroup pg, String username) {
        float userSum = 0f;
        float sum = 0f;
        for (Payment it : pg.getTransactions()) {
            if (it.getUser().equals(username)) {
                userSum += it.getAmount().floatValue();
            }
            sum += it.getAmount().floatValue();
        }
        float avg = sum / Integer.valueOf(pg.getUsers().size()).floatValue();
        return avg - userSum;
    }
}
