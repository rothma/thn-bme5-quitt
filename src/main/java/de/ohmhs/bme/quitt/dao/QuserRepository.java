package de.ohmhs.bme.quitt.dao;

import de.ohmhs.bme.quitt.model.Quser;

/**
 * Repository implementation for work on {@link Quser} Documents in the MongoDB
 */
public class QuserRepository extends AbstractRepository<Quser> {

    // this property provides the collection name where Quser Documents are stored
    public static final String COLLECTION_NAME = "qusers";

    /**
     * Constructor for a QuserRepository.
     *
     * @param connection the MongoConnection instances that encapsules the MongoDB Database Connection
     * @throws ClassNotFoundException in case when Quser Class is not present at runtime
     */
    public QuserRepository(MongoConnection connection) throws ClassNotFoundException {
        super(connection, COLLECTION_NAME);
    }

    /**
     * queries the collection 'qusers' for a Quser instance with the given username
     *
     * @param username of the Quser
     * @return the Quser instance with matching username
     */
    public Quser getUser(String username) {
        return find("username", username).get(0);
    }
}
