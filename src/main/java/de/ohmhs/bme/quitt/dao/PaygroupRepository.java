package de.ohmhs.bme.quitt.dao;

import de.ohmhs.bme.quitt.model.Paygroup;

import java.util.List;

/**
 * Repository implementation for work on {@link Paygroup} Documents in the MongoDB
 */
public class PaygroupRepository extends AbstractRepository<Paygroup> {

    // this property provides the collection name where Paygroup Documents are stored
    public static final String COLLECTION_NAME = "paygroups";

    /**
     * Constructor for a PaygroupRepository.
     *
     * @param connection the MongoConnection instances that encapsules the MongoDB Database Connection
     * @throws ClassNotFoundException in case when Paygroup Class is not present at runtime
     */
    public PaygroupRepository(MongoConnection connection) throws ClassNotFoundException {
        super(connection, COLLECTION_NAME);
    }

    /**
     * queries collection 'paygroups' for Paygroup instances where the given username is a member of
     *
     * @param username the username acting as search criterion
     * @return a list of paygroups where the Quser is member of
     */
    public List<Paygroup> getPaygroupsForUsername(String username) {
        return find("users", username);
    }

    /**
     * queries collection 'paygroups' for Paygroup instances with given title
     *
     * @param title the title of the paygroup that should be looked up
     * @return the paygroup with the given title
     */
    public Paygroup getPaygroupForTitle(String title) {
        return find("title", title).get(0);
    }
}
