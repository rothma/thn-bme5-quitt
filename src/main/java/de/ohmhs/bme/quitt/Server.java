package de.ohmhs.bme.quitt;

/**
 * Main Class of the Quitt Application.
 */
public class Server {

    /**
     * main method that bootstraps the quitt app by creating an {@link AppBuilder} with an instance of
     * {@link AppFactory} and calling the build method.
     *
     * @param args arguments provided by the calling context (currently not in use)
     */
    public static void main(String[] args) {
        AppBuilder.create().withFactory(AppFactory.getInstance()).build();
    }
}
