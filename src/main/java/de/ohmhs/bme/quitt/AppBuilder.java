package de.ohmhs.bme.quitt;

import de.ohmhs.bme.quitt.controller.PaygroupController;
import de.ohmhs.bme.quitt.controller.SessionHandler;
import de.ohmhs.bme.quitt.dao.MongoConnection;
import de.ohmhs.bme.quitt.dao.PaygroupRepository;
import de.ohmhs.bme.quitt.dao.QuserRepository;
import io.javalin.Javalin;

import java.util.HashMap;
import java.util.Map;

/**
 * Builder class for this application. created from a static call of 'create', it consumes a {@link IFactory}
 * that is used to instantiate Object Instances. All of this instances are put into a Map (objects)
 * and participants could lookup them via the 'getBean' method
 */
public class AppBuilder {

    private static AppBuilder _instance;
    private IFactory factory = null;
    private Map<String, Object> objects = new HashMap<>();


    /**
     * static create function (singleton)
     *
     * @return a {@link AppBuilder} instance
     */
    public static AppBuilder create() {
        if (_instance == null) {
            _instance = new AppBuilder();
        }
        return _instance;
    }

    /**
     * configures this builder with an IFactory instance
     *
     * @param factory used to construct object instances
     * @return this {@link AppBuilder} instance
     */
    public AppBuilder withFactory(IFactory factory) {
        this.factory = factory;
        return this;
    }

    /**
     * builder method that takes care of instantiation of objects (via call to factory methods)
     * and build up the object graph of the entire app (taking care of the exact order of creation and
     * compositing of aggregations).
     * created object instances are put into the 'objects' map
     *
     * @return the {@link AppBuilder} instance
     */
    public AppBuilder build() {
        MongoConnection con = new MongoConnection("quitt", "quitt", "quitt");
        this.objects.put(MongoConnection.class.getSimpleName(), con);
        try {
            PaygroupRepository pgr = (PaygroupRepository) factory.createRepository(PaygroupRepository.COLLECTION_NAME, con);
            this.objects.put(PaygroupRepository.class.getSimpleName(), pgr);
            QuserRepository qur = (QuserRepository) factory.createRepository(QuserRepository.COLLECTION_NAME, con);
            this.objects.put(QuserRepository.class.getSimpleName(), qur);
            SessionHandler sh = factory.createSessionHandler(qur);
            this.objects.put(SessionHandler.class.getSimpleName(), sh);
            PaygroupController pgc = (PaygroupController) factory.createController("Paygroup", pgr, qur);
            this.objects.put(PaygroupController.class.getSimpleName(), pgc);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
        Javalin app = Javalin.create();
        this.objects.put(Javalin.class.getSimpleName(), app);
        AppConfiguration apc = new AppConfiguration(this);
        this.objects.put(AppConfiguration.class.getSimpleName(), apc);
        apc.configure();
        app.start();
        return this;
    }

    /**
     * retrieves an object from the 'objects' map by name.
     *
     * @param name of the object to be retrieved (key is Class.getSimpleName())
     * @return the object instance (singleton) requested (has to be casted to right Type)
     * @throws InstantiationException if the Object cannot be found by name
     */
    public Object getBean(String name) throws InstantiationException {
        Object toRet = objects.get(name);
        if (toRet == null) {
            throw new InstantiationException("bean with name " + name + " could not be found in app context");
        }
        return toRet;
    }
}
