package de.ohmhs.bme.quitt;

import de.ohmhs.bme.quitt.controller.AbstractController;
import de.ohmhs.bme.quitt.controller.PaygroupController;
import de.ohmhs.bme.quitt.controller.SessionHandler;
import de.ohmhs.bme.quitt.dao.IRepository;
import de.ohmhs.bme.quitt.dao.MongoConnection;
import de.ohmhs.bme.quitt.dao.PaygroupRepository;
import de.ohmhs.bme.quitt.dao.QuserRepository;

/**
 * Implementaion of the {@link IFactory} interface that takes care of creation of objects
 */
public class AppFactory implements IFactory {
    // static reference of this factory
    private static AppFactory _instance = null;

    /**
     * getter for this Singleton instance (creation on demand)
     *
     * @return an instance of this AppFactory Class
     */
    public static AppFactory getInstance() {
        if (_instance == null) {
            _instance = new AppFactory();
        }
        return _instance;
    }

    /**
     * evaluates the given name and calls the repository constructors (the collection name as parameter)
     *
     * @param name            of the Repository to create
     * @param mongoConnection the MongoConnection that is shared across all repositories
     * @return a subclass of {@link IRepository} with the shared {@link MongoConnection}
     * @throws ClassNotFoundException if the combination of name and mongoConnection could not be handled by this method
     */
    @SuppressWarnings("rawtypes")
    @Override
    public IRepository createRepository(String name, MongoConnection mongoConnection) throws ClassNotFoundException {
        if (name.equals(QuserRepository.COLLECTION_NAME)) {
            return new QuserRepository(mongoConnection);
        }
        if (name.equals(PaygroupRepository.COLLECTION_NAME)) {
            return new PaygroupRepository(mongoConnection);
        }
        throw new ClassNotFoundException();
    }

    /**
     * creates controllers and configures them with repositores to use for database interactions
     *
     * @param name of the Controller to create
     * @param repo one or more {@link IRepository} instances this controller can use to coordinate data access
     * @return the subclass of {@link AbstractController} tha corresponds to the given name
     */
    @SuppressWarnings("rawtypes")
    @Override
    public AbstractController createController(String name, IRepository... repo) {
        if (name.equals("Paygroup")) {
            return new PaygroupController((PaygroupRepository) repo[0], (QuserRepository) repo[1]);
        }
        return null;
    }

    @Override
    public SessionHandler createSessionHandler(QuserRepository repo) {
        return new SessionHandler(repo);
    }

}
