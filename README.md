Beispielanwendung quitt
=======================
Dieses Repository beinhaltet den Quelltext für die Übung aus der Lehrveranstaltung 
Internetprogrammierung an der Technischen Hochschule Nürnberg.

Die Webanwendung ermöglich es Benutzern nach erfolgter Authentifizierung das Anlegen
und Verwalten von Gruppen, bei denen die Anwender einen Überblick über eine gemeinsam
geführte Haushaltskasse haben können, in dem jeder Beteiligte Einzahlungen für die
Gemeinschaft quittieren und einsehen können, damit bei der Auflösung der Gruppe jeder
weiß, ob noch eine Zahlung zu leisten oder zu erwarten ist.

Dependencies
---------------
Die Anwendung setzt für das Kompilieren und Ausführen das Vorhandensein
eines Java SDK's (>= Version 11) voraus. Ich empfehle die Installation des 
[Adopt OpenJDK](https://adoptopenjdk.net), da diese Downloads für alle gängigen
Betriebssysteme gepflegt und mit Installroutinen ausgeliefert werden.


Die Anwendung benutzt folgende Bibliotheken (Frameworks):
* [Javalin](https://javalin.io) als Web-Microframework mit embedded Jetty Servlet-Container
und einfacher API für serverseitige Programmierung von HTTP und Websockets
* [Thymeleaf](https://www.thymeleaf.org) als Templating-Engine zur Erstellung dynamisscher
serverseitiger HTML-Ausgaben
* [Mongo-Java-Driver](https://mongodb.github.io/mongo-java-driver/) für die Konnektivität zu
MongoDB Instanzen in Java-Anwendungen.

Datenbank
---------
Voraussetzung für den Betrieb ist eine installierte MongoDB instanz, die mit einer
eigenen Datenbank und einem Benutzer darin mit ReadWrite Berechtigungen vorbereitet
worden ist. Bei einer frisch installierten Instanz ist dazu in der Mongo-Shell
folgendes auszuführen:

```
mongo
> use quitt
```

Dieses Kommando erstellt die Datenbank "quitt" und wechselt den Kontext gleichzeitig auf
diese DB.

`db.createUser({user: "quitt", pwd: "quitt", roles: [{role: "readWrite", db: "quitt"}]})` 

Dieses Kommando erstellt einen Datenbankbenutzer "quitt" mit dem Passwort "quitt" und
weißt die Berechtigungen für das Lesen und Schreiben auf die Datenbank "quitt" zu.

Nach Verlassen der Shell kann das Funktionieren mit einer authentifizierten Shell
ausprobiert und die notwendigen Collections angelegt werden:

```
mongo -u quitt -p --authenticationDatabase quitt
> use quitt
> db.createCollection("paygroups")`
> db.createCollection("qusers")
```

Dieses Repository beinhaltet einen Beispieldatensatz pro Collection. Sie können über
das Tool `mongoimport` geladen werden:

```
mongoexport -u quitt -p quitt --authenticationDatabase quitt --db quitt --collection paygroups --file src/main/mongo/paygroups.json
mongoexport -u quitt -p quitt --authenticationDatabase quitt --db quitt --collection qusers --file src/main/mongo/qusers.json
```

Bauen und Ausführung der Anwendung
-------------------
Da dieses Projekt [maven](https://maven.apache.org) verwendet, ist eine Installation von
Apache maven (standalone oder in der IDE integriert) notwendig. im Wurzelverzeichnisses
dieses Projektes genügt zum Bauen folgender Befehl

```
mvn package
```
Dadurch wird die Anwendung als Self-Contained-App gebaut und im
generierten Verzeichnis 'target' abgelegt.

Das Ausführen kann in der Kommandozeile erfolgen durch:
```
cd target
java -jar quitt-full.jar
```

Aufbau und Design
-----------------
Die Anwendung ist in folgende Packages untergliedert:
* de.ohmhs.bme.quitt:  MainClass und Factory und Builders (vgl. Creational Patterns)
![UML Diagramm App-Classes](src/site/img/UML-App-quitt-Builders.png)
* de.ohmhs.bme.quitt.controller: Controller, die HTTP-Endpoint-Handler ausgestalten und
die Business-Logik implementieren.
* de.ohmhs.bme.quitt.dao: Repositories, die unter Verwendung einer Datenbankverbindung
die Daten aus ihr lesen und manipulieren und die Ergebnisse an die aufrufenden Controller
weiterreichen
![UML Diagramm App-Classes](src/site/img/UML-App-quitt.png)
* de.ohmhs.bme.quitt.model: MongoDB POJOs, die die Datenbank-Dokumente als Java-Objekte
abbilden und in den Repositories und Controllers als Daten-Transfer-Objekte benutzt werden 

Dokumentation
-------------
Alle Klassen und auch die pom.xml sind kommentiert, so dass an den betreffenden Codestellen
Funktionsweise und weiterführende Kontextinformation dort entnommen werden können.